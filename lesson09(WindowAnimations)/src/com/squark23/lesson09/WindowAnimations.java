package com.squark23.lesson09;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by squark23 on 6/20/14.
 */
public class WindowAnimations extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_window_animations);


        final Button defaultButton = (Button) findViewById(R.id.defaultButton);
        final Button translateButton = (Button) findViewById(R.id.translateButton);
        final Button scaleButton = (Button) findViewById(R.id.scaleButton);
        final ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);

        defaultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent subActivity = new Intent(WindowAnimations.this, SubActivity.class);
                startActivity(subActivity);
            }
        });

        translateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent subActivity = new Intent(WindowAnimations.this, AnimatedSubActivity.class);
                Bundle translateBundle = ActivityOptions.makeCustomAnimation(WindowAnimations.this,
                        R.anim.slide_in_left, R.anim.slide_out_left).toBundle();
                startActivity(subActivity, translateBundle);
            }
        });

        scaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent subActivity = new Intent(WindowAnimations.this, AnimatedSubActivity.class);
                Bundle scaleBundle = ActivityOptions.makeScaleUpAnimation(
                        v, 0, 0, v.getWidth(), v.getHeight()).toBundle();
                startActivity(subActivity, scaleBundle);
            }
        });

        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BitmapDrawable drawable = (BitmapDrawable) thumbnail.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                Intent subActivity = new Intent(WindowAnimations.this, AnimatedSubActivity.class);
                Bundle scaleBundle = ActivityOptions.makeThumbnailScaleUpAnimation(
                        thumbnail, bitmap, 0, 0).toBundle();
                startActivity(subActivity, scaleBundle);
            }
        });
    }
}
