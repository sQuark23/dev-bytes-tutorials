package com.squark23.lesson09;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by squark23 on 6/20/14.
 */
public class SubActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
    }
}
