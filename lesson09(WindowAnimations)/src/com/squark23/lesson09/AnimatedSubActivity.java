package com.squark23.lesson09;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by squark23 on 6/20/14.
 */
public class AnimatedSubActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_window_anim_sub);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
