package com.squark23.lesson10;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by squark23 on 6/20/14.
 */
public class BitmapAllocation extends Activity {

    int mCurrentIndex = 0;
    Bitmap mCurrentBitmap = null;
    BitmapFactory.Options mBitmapOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitmap_allocation);

        final int[] imageIDs = {R.drawable.a, R.drawable.b, R.drawable.c, R.drawable.d,
                R.drawable.e, R.drawable.f};

        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox);
        final TextView durationTextView = (TextView) findViewById(R.id.loadDuration);
        final ImageView imageView = (ImageView) findViewById(R.id.imageview);

        mBitmapOptions = new BitmapFactory.Options();
        mBitmapOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), R.drawable.a, mBitmapOptions);
        mCurrentBitmap = Bitmap.createBitmap(mBitmapOptions.outWidth,
                mBitmapOptions.outHeight, Bitmap.Config.ARGB_8888);
        mBitmapOptions.inJustDecodeBounds = false;
        mBitmapOptions.inBitmap = mCurrentBitmap;
        mBitmapOptions.inSampleSize = 1;
        BitmapFactory.decodeResource(getResources(), R.drawable.a, mBitmapOptions);
        imageView.setImageBitmap(mCurrentBitmap);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentIndex = (mCurrentIndex + 1) % imageIDs.length;
                BitmapFactory.Options bitmapOptions = null;

                if (checkBox.isChecked()) {
                    bitmapOptions = mBitmapOptions;
                    bitmapOptions.inBitmap = mCurrentBitmap;
                }

                long startTime = System.currentTimeMillis();
                mCurrentBitmap = BitmapFactory.decodeResource(getResources(), imageIDs[mCurrentIndex], bitmapOptions);
                imageView.setImageBitmap(mCurrentBitmap);

                durationTextView.setText("Load took " + (System.currentTimeMillis() - startTime));
            }
        });

    }
}
