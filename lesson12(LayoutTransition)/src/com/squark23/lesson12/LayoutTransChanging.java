package com.squark23.lesson12;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.view.View;


/**
 * Created by squark23 on 6/20/14.
 */
public class LayoutTransChanging extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        final Button addButton = (Button) findViewById(R.id.addButton);
        final Button removeButton = (Button) findViewById(R.id.removeButton);
        final LinearLayout container = (LinearLayout) findViewById(R.id.container);
        final Context context = this;

        for (int i = 0; i < 2; i++) {
            container.addView(new ColoredView(this));
        }

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container.addView(new ColoredView(context), 1);
            }
        });

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (container.getChildCount() > 0)
                    container.removeViewAt(Math.min(1, container.getChildCount() - 1));
            }
        });

        LayoutTransition transition = container.getLayoutTransition();
        transition.enableTransitionType(LayoutTransition.CHANGING);
    }

    private static class ColoredView extends View {

        private boolean mExpanded = false;

        private LayoutParams mCompressedParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, 50);

        private LayoutParams mExpandedParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, 200);

        private ColoredView(Context context) {
            super(context);
            int red = (int)(Math.random() * 128 + 127);
            int green = (int)(Math.random() * 128 + 127);
            int blue = (int)(Math.random() * 128 + 127);
            int color = 0xff << 24 | (red << 16) |
                    (green << 8) | blue;
            setBackgroundColor(color);
            setLayoutParams(mCompressedParams);
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Size changes will cause a LayoutTransition animation if the CHANGING
                    // transition is enabled
                    setLayoutParams(mExpanded ? mCompressedParams : mExpandedParams);
                    mExpanded = !mExpanded;
                    requestLayout();
                }
            });
        }
    }
}

