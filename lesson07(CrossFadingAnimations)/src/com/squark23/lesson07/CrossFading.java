package com.squark23.lesson07;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by squark23 on 6/19/14.
 */
public class CrossFading extends Activity {

    int mCurrentDrawable = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cross_fading);

        final ImageView imageView = (ImageView) findViewById(R.id.imageview);

        Bitmap bitmap0 = Bitmap.createBitmap(900, 900, Bitmap.Config.ARGB_8888);
        Bitmap bitmap1 = Bitmap.createBitmap(900, 900, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap0);
        canvas.drawColor(Color.RED);
        canvas = new Canvas(bitmap1);
        canvas.drawColor(Color.GREEN);

        BitmapDrawable[] drawables = new BitmapDrawable[2];
        drawables[0] = new BitmapDrawable(getResources(), bitmap0);
        drawables[1] = new BitmapDrawable(getResources(), bitmap1);

        final TransitionDrawable crossfade = new TransitionDrawable(drawables);
        imageView.setImageDrawable(crossfade);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentDrawable == 0) {
                    crossfade.startTransition(500);
                    mCurrentDrawable = 1;
                } else {
                    crossfade.reverseTransition(500);
                    mCurrentDrawable = 0;
                }
            }
        });
    }
}
