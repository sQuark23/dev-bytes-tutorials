package com.squark23.lesson11;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by squark23 on 6/20/14.
 */
public class PictureViewer extends Activity {

    int mCurrentDrawable = 0;
    int[] drawableIDs = {
            R.drawable.p1,
            R.drawable.p2,
            R.drawable.p3,
            R.drawable.p4,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_viewer);

        final ImageView prevImageView = (ImageView) findViewById(R.id.prevImageView);
        final ImageView nextImageView = (ImageView) findViewById(R.id.nextImageView);
        prevImageView.setBackgroundColor(Color.TRANSPARENT);
        nextImageView.setBackgroundColor(Color.TRANSPARENT);

        prevImageView.animate().setDuration(1000);
        nextImageView.animate().setDuration(1000);

        final BitmapDrawable[] drawables = new BitmapDrawable[drawableIDs.length];
        for (int i = 0; i < drawables.length; i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                    drawableIDs[i]);
            drawables[i] = new BitmapDrawable(getResources(), bitmap);
        }

        prevImageView.setImageDrawable(drawables[0]);
        nextImageView.setImageDrawable(drawables[1]);

        prevImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevImageView.animate().alpha(0).withLayer();
                nextImageView.animate().alpha(1).withLayer()
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                mCurrentDrawable = (mCurrentDrawable + 1) % drawables.length;
                                int nextDrawableIndex = (mCurrentDrawable + 1) % drawables.length;

                                prevImageView.setImageDrawable(drawables[mCurrentDrawable]);
                                nextImageView.setImageDrawable(drawables[nextDrawableIndex]);
                                nextImageView.setAlpha(0f);
                                prevImageView.setAlpha(1f);
                            }
                        });
            }
        });
    }
}

