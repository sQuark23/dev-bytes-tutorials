package com.squark23.lesson01;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        LinearLayout container = (LinearLayout) findViewById(R.id.scaledImageContainer);
        ImageView originalImageView = (ImageView) findViewById(R.id.originalImageHolder);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.jellybean_statue);
        originalImageView.setImageBitmap(bitmap);

        for (int i = 2; i < 10; i++) {
            addScaledImageView(bitmap, i, container);
        }

    }

    private void addScaledImageView(Bitmap original, int sampleSize, LinearLayout container) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = sampleSize;

        Bitmap scaledBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.jellybean_statue, bitmapOptions);

        ImageView scaledImageView = new ImageView(this);
        scaledImageView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        scaledImageView.setImageBitmap(scaledBitmap);
        container.addView(scaledImageView);

    }
}
