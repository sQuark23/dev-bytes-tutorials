package com.squark23.lesson04;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.*;
import android.widget.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by squark23 on 6/17/14.
 */
public class ListViewDeletion extends Activity {

    final ArrayList<View> mCheckedViews = new ArrayList<View>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_deletion);

        final Button deleteButton = (Button) findViewById(R.id.deleteButton);
        final CheckBox usePositionsCB = (CheckBox) findViewById(R.id.usePositionsCB);
        final ListView listview = (ListView) findViewById(R.id.listview);
        final ArrayList<String> cheeseList = new ArrayList<String>();

        for (int i = 0; i < Cheeses.sCheeseStrings.length; i++) {
            cheeseList.add(Cheeses.sCheeseStrings[i]);
        }

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_multiple_choice, cheeseList);

        listview.setAdapter(adapter);
        listview.setItemsCanFocus(false);
        listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SparseBooleanArray checkedItems = listview.getCheckedItemPositions();
                int numCheckedItems = checkedItems.size();

                for (int i = numCheckedItems - 1; i >= 0; --i) {
                    if (!checkedItems.valueAt(i))
                        continue;

                    int position = checkedItems.keyAt(i);
                    final String item = adapter.getItem(position);
                    if (!usePositionsCB.isChecked()) {
                        v.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                adapter.remove(item);
                            }
                        }, 300);
                    } else {
                        mCheckedViews.clear();
                        int positionOnScreen = position - listview.getFirstVisiblePosition();
                        if (positionOnScreen >= 0 && positionOnScreen < listview.getChildCount()) {
                            final View view = listview.getChildAt(positionOnScreen);

                            view.animate().alpha(0).withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    view.setAlpha(1);
                                    adapter.remove(item);
                                }
                            });
                        } else {
                            v.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.remove(item);
                                }
                            }, 300);
                        }
                    }
                }

                for (int i = 0; i < mCheckedViews.size(); ++i) {
                    final View checkedView = mCheckedViews.get(i);
                    checkedView.animate().alpha(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            checkedView.setAlpha(1);
                        }
                    });
                }

                mCheckedViews.clear();
                adapter.notifyDataSetChanged();
            }
        });

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean checked = listview.isItemChecked(position);
                if (checked)
                    mCheckedViews.add(view);
                else
                    mCheckedViews.remove(view);
            }
        });

    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
}
