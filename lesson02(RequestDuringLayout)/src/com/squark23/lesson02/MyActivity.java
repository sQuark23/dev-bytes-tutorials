package com.squark23.lesson02;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.*;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final MyLayout myLayout = (MyLayout) findViewById(R.id.container);
        Button addViewButton = (Button) findViewById(R.id.addView);
        Button removeViewButton = (Button) findViewById(R.id.removeView);
        Button forceLayoutButton = (Button) findViewById(R.id.forceLayout);


        addViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myLayout.mAddRequestPending = true;
                myLayout.requestLayout();
            }
        });

        removeViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myLayout.mRemoveRequestPending = true;
                myLayout.requestLayout();
            }
        });

        forceLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myLayout.requestLayout();
            }
        });
    }

    static class MyLayout extends LinearLayout {

        int numButtons = 0;
        boolean mAddRequestPending = false;
        boolean mRemoveRequestPending = false;

        public MyLayout(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        public MyLayout(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public MyLayout(Context context) {
            super(context);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
            super.onLayout(changed, l, t, r, b);

            if (mRemoveRequestPending) {
                removeButton();
                mRemoveRequestPending = false;
            }

            if (mAddRequestPending) {
                addButton();
                mAddRequestPending = false;
            }
        }

        private void removeButton() {
            if (getChildCount() > 1) {
                removeViewAt(1);
            }
        }

        private void addButton() {
            Button button = new Button(getContext());
            button.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            button.setText("Button " + (numButtons++));
            addView(button);
        }
    }
}
